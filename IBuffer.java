package ru.bellido.myexamples;

public interface IBuffer {
	String getNextLine();
	void putLine(String line);
}
