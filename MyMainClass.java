package ru.bellido.myexamples;

public class MyMainClass {

	public static void main(String[] args) {
		ICondition condition = new ConditionBlackList("�");
		IBuffer buffer = new BufferList(1024);
		WriterThread writer = new WriterThread(buffer);
		writer.registeredCondition(condition);
		ReaderThread reader = new ReaderThread("data.txt", buffer);
		
		new Thread(reader).start();
		new Thread(writer).start();
	}
}
