package ru.bellido.myexamples;

public interface ICondition {
	boolean isSatisfy(String word);
}
