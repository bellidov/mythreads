package ru.bellido.myexamples;

public class WriterThread implements Runnable
{
	IBuffer buffer;
	ICondition condition;
	String[] words;
	
	public WriterThread(IBuffer buffer){
		this.buffer = buffer;
	}
	@Override
	public void run() {
		TextAnalizerByWords();
		System.out.println("Writer Thread finished!");
	}
	
	public void registeredCondition(ICondition c) {
		this.condition = c;
	}

	public void TextAnalizerByWords() {
		while(true) {
			words = getWordsFromBuffer();
			for(String word : words){
				if(condition.isSatisfy(word)){
					putToFile(word);
				}
			}
		}
	}
	
	private String[] getWordsFromBuffer(){
		return buffer.getNextLine().split(" ");
	}

	public void putToFile(String myWord) {
		System.out.println("Eto slovo kotoroe my ishem: " + myWord);
	}


}
