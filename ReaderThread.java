package ru.bellido.myexamples;

import java.io.*;

public class ReaderThread implements Runnable
{
	private BufferedReader reader;
	private IBuffer buffer;
	
	public ReaderThread(String fileName, IBuffer buffer){
		this.buffer = buffer;
		try {
			reader = new BufferedReader( new FileReader(fileName));
		} catch (FileNotFoundException e) {
			System.out.println("file not found");
		}
	}
	
	@Override
	public void run() {
		getFromFileAndPutToBuffer();
		System.out.println("Reader Thread finished!");
	}
	
	public void getFromFileAndPutToBuffer() {
		String temp = null;
		try {
			while((temp = reader.readLine()) != null){
				buffer.putLine(temp);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
