package ru.bellido.myexamples;

public class ConditionBlackList implements ICondition
{

	private String keyWord;
	
	public ConditionBlackList(String keyWord) {
		this.keyWord = keyWord;
	}
	@Override
	public boolean isSatisfy(String word) {
		return word.equalsIgnoreCase(keyWord);
	}

}
