package ru.bellido.myexamples;

import java.util.LinkedList;
import java.util.List;

public class BufferList implements IBuffer
{

	private List<String> words;
	private int maxSize;
	private Object locker = new Object();
	
	public BufferList(int maxSize) {
		this.maxSize = maxSize;
		words = new LinkedList();
	}
	
	public String getNextLine() {
		String line = null;
		synchronized (locker){
			while(words.isEmpty()){
			 try {
				locker.wait();
			  } catch (InterruptedException e) {
				e.printStackTrace();
			   }
		    }
		    line = words.remove(0);
		    locker.notifyAll();
		}
		return line;
	}

	public void putLine(String line) {
		synchronized (locker) {

			while(words.size() >= maxSize){
				try {
					locker.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			words.add(line);
			locker.notifyAll();
		}
	}

}
